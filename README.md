Teaboard
===============

Teaboard is a free software on-screen keyboard and input method engine, with
extensive features, and a focus on multilingual typing and use in multiple
input scenarios, such as touch screen only, combined touch screen when using
a physical keyboard as on a tablet with keyboard case, when using only a
physical keyboard as on a traditional PC, or using a gamepad or remote to
select what keys to type.

Teaboard is a combined fork of the Maliit framework and keyboard, which itself
was forked and based on the Ubuntu and Lomiri keyboard, which was originally
based on the reference plugin for the Maliit framework. These components have
been merged into a single repository to form Teabord, which aims to greatly
simplify the keyboard, and provide it as a single cohesive unit. As such, some
features such as X11 support, the dbus protocol, and input-context plugins have
been removed. Teaboard only supports modern display systems, such as Wayland.


Compiling
---------

Teaboard is bulit with Qt. It can currently be bulit with either Qt6 or Qt5.
Anthy is required for Japanese input, and libpinyin/libzhuyin are required for
Chinese. For spell checking, Hunspell is needed. To build with Qt6, you need
to run `cmake` with the `-Dwith-qt6=ON` option.


License
-------
The license of the combined work is GPL-3.0-only.

The license of the code from maliit-framework is LGPL-2.1-only, as described
in `COPYING.LGPLv2`.

The majority of individual files in `src` are under a BSD license as written
in `COPYING.BSD`.

The majority of individual files in `qml` are under LGPL-3.0-only.

New code should generally be licensed as GPL-3.0-only.
