/* * This file is part of Maliit framework *
 *
 * Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved.
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * and appearing in the file LICENSE.LGPL included in the packaging
 * of this file.
 */

#include "core-utils.h"

#include <QtDebug>
#include <QtCore>
#include <QtTest>

namespace {
    const QString TestingInSandboxEnvVariable("TESTING_IN_SANDBOX");
}

namespace MaliitTestUtils {

/* Return true if we are testing against the repository tree,
or false if testing against installed software. */
bool isTestingInSandbox()
{
    bool testingInSandbox = false;
    const QStringList env(QProcess::systemEnvironment());
    int index = env.indexOf(QRegularExpression('^' + TestingInSandboxEnvVariable + "=.*", QRegularExpression::CaseInsensitiveOption));
    if (index != -1) {
        QString statusCandidate = env.at(index);
        statusCandidate = statusCandidate.remove(QRegularExpression('^' + TestingInSandboxEnvVariable + '=', QRegularExpression::CaseInsensitiveOption));
        bool statusOk = false;
        int status = statusCandidate.toInt(&statusOk);
        if (statusOk && (status == 0 || status == 1)) {
            testingInSandbox = (status == 1);
        } else {
            qCritical() << "Invalid " << TestingInSandboxEnvVariable << " environment variable.\n";
        }
    }
    return testingInSandbox;
}

// Wait for signal or timeout; use SIGNAL macro for signal
void waitForSignal(const QObject* object, const char* signal, int timeout)
{
    QEventLoop eventLoop;
    QObject::connect(object, signal, &eventLoop, SLOT(quit()));
    QTimer::singleShot(timeout, &eventLoop, SLOT(quit()));
    eventLoop.exec();
}

}
