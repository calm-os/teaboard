#=============================================================================
# SPDX-FileCopyrightText: 2012-2014 Pier Luigi Fiorini <pierluigi.fiorini@gmail.com>
#
# SPDX-License-Identifier: BSD-3-Clause
#=============================================================================

# Find qmllint
find_program(QmlLint_EXECUTABLE
  NAMES
    qmllint
  HINTS
    /usr/lib64/qt${QT_VERSION_MAJOR}/bin/
    /usr/lib/qt${QT_VERSION_MAJOR}/bin/
    /usr/lib/qt${QT_VERSION_MAJOR}/libexec/
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(QmlLint
    FOUND_VAR
        QmlLint_FOUND
    REQUIRED_VARS
        QmlLint_EXECUTABLE
)

mark_as_advanced(QmlLint_EXECUTABLE)

if(NOT TARGET Qml::Lint AND QmlLint_FOUND)
    add_executable(Qml::Lint IMPORTED)
    set_target_properties(Qml::Lint PROPERTIES
        IMPORTED_LOCATION "${QmlLint_EXECUTABLE}"
    )
endif()

include(FeatureSummary)
set_package_properties(QmlLint PROPERTIES
    URL "https://qt.io/"
    DESCRIPTION "Executable that validates QML code"
)
