/*
 * This file is part of Maliit Plugins
 *
 * Copyright (C) 2013 Canonical, Ltd.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 * Neither the name of Nokia Corporation nor the names of its contributors may be
 * used to endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "keyboardsettings.h"

#include <QDebug>


using namespace MaliitKeyboard;

const QLatin1String ACTIVE_LANGUAGE_KEY = QLatin1String("activeLanguage");
const QLatin1String ENABLED_LANGUAGES_KEY = QLatin1String("enabledLanguages");
const QLatin1String AUTO_CAPITALIZATION_KEY = QLatin1String("autoCapitalization");
const QLatin1String AUTO_COMPLETION_KEY = QLatin1String("autoCompletion");
const QLatin1String PREDICTIVE_TEXT_KEY = QLatin1String("predictiveText");
const QLatin1String SPELL_CHECKING_KEY = QLatin1String("spellChecking");
const QLatin1String KEY_PRESS_AUDIO_FEEDBACK_KEY = QLatin1String("keyPressFeedback");
const QLatin1String KEY_PRESS_AUDIO_FEEDBACK_SOUND_KEY = QLatin1String("keyPressFeedbackSound");
const QLatin1String KEY_PRESS_HAPTIC_FEEDBACK_KEY = QLatin1String("keyPressHapticFeedback");
const QLatin1String ENABLE_MAGNIFIER_KEY = QLatin1String("enableMagnifier");
const QLatin1String DOUBLE_SPACE_FULL_STOP_KEY = QLatin1String("doubleSpaceFullStop");

/*!
 * \brief KeyboardSettings::KeyboardSettings class to load the settings, and
 * listens on runtime to changes of them
 * \param parent
 */
KeyboardSettings::KeyboardSettings(const QSettings& settings, QObject *parent)
    : QObject(parent)
    , m_settings(settings.fileName(), QSettings::IniFormat)
{
}

/*!
 * \brief KeyboardSettings::activeLanguage returns currently active language
 * \return active language
 */

QString KeyboardSettings::activeLanguage() const
{
    return "en";
}

void KeyboardSettings::setActiveLanguage(const QString& id)
{
}

void KeyboardSettings::resetActiveLanguage()
{
}

/*!
 * \brief KeyboardSettings::enabledLanguages returns a list of languages that are
 * active
 * \return
 */
QStringList KeyboardSettings::enabledLanguages() const
{
    return QStringList() << "en";
}

void KeyboardSettings::setEnabledLanguages(const QStringList& ids)
{
    if (ids.isEmpty()) {
        resetEnabledLanguages();
        return;
    }
}

void KeyboardSettings::resetEnabledLanguages()
{
}

/*!
 * \brief KeyboardSettings::autoCapitalization returns true id the first letter
 * of each sentence should be capitalized
 * \return
 */
bool KeyboardSettings::autoCapitalization() const
{
    return true;
}

/*!
 * \brief KeyboardSettings::autoCompletion returns true if the current word should
 * be completed with first suggestion when hitting space
 * \return
 */
bool KeyboardSettings::autoCompletion() const
{
    return true;
}

/*!
 * \brief KeyboardSettings::predictiveText returns true, if potential words in the
 * word ribbon should be suggested
 * \return
 */
bool KeyboardSettings::predictiveText() const
{
    return true;
}

/*!
 * \brief KeyboardSettings::spellchecking returns true if spellchecking should be used
 * \return
 */
bool KeyboardSettings::spellchecking() const
{
    return true;
}

/*!
 * \brief KeyboardSettings::keyPressAudioFeedback returns true if audio feedback is enabled
 * when the user presses a keyboad key
 * \return
 */
bool KeyboardSettings::keyPressAudioFeedback() const
{
    return false;
}

/*!
 * \brief KeyboardSettings::keyPressHapticFeedback returns true if haptic feedback is enabled
 * when the user presses a keyboad key
 * \return
 */
bool KeyboardSettings::keyPressHapticFeedback() const
{
    return true;
}

/*!
 * \brief KeyboardSettings::enableMagnifier returns true if magnifier is enabled
 * when the user presses a keyboard key
 * \return
 */
bool KeyboardSettings::enableMagnifier() const
{
    return true;
}

/*!
 * \brief KeyboardSettings::keyPressFeedbackSound returns the path to the current key
 * feedback sound
 * \return path to the feedback sound
 */
QString KeyboardSettings::keyPressAudioFeedbackSound() const
{
    return "";
}

/*!
 * \brief KeyboardSettings:doubleSpaceFullStop returns true if double space full-stops are
 * enabled, which insert a full-stop when the space key is pressed twice.
 */
bool KeyboardSettings::doubleSpaceFullStop() const
{
    return true;
}
