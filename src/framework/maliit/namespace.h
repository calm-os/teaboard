/* * This file is part of Maliit framework *
 *
 * Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved.
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * and appearing in the file LICENSE.LGPL included in the packaging
 * of this file.
 */

#ifndef MALIIT_NAMESPACE_H
#define MALIIT_NAMESPACE_H

#include <QMetaType>
#include <QSharedPointer>

//! \ingroup common
namespace Maliit {
    /*!
     * \brief Position of the window on the screen.
     */
    enum Position {
        PositionOverlay,
        PositionCenterBottom,
        PositionLeftBottom,
        PositionRightBottom,
    };

    /*!
     * \brief Content type for text entries.
     *
     * Content type of the text in the text edit widget, which can be used by
     * input method plugins to offer more specific input methods, such as a
     * numeric keypad for a number content type. Plugins may also adjust their
     * word prediction and error correction accordingly.
     */
    enum TextContentType {
        //! all characters allowed
        FreeTextContentType,

        //! only integer numbers allowed
        NumberContentType,

        //! only numbers and formatted characters
        FormattedNumberContentType,

        //! allows numbers and certain other characters used in phone numbers
        PhoneNumberContentType,

        //! allows only characters permitted in email address
        EmailContentType,

        //! allows only character permitted in URL address
        UrlContentType,

        //! allows content with user defined format
        CustomContentType
    };

    /*!
     * \brief State of Copy/Paste button.
     */
    enum CopyPasteState {
        //! Copy/Paste button is hidden
        InputMethodNoCopyPaste,

        //! Copy button is accessible
        InputMethodCopy,

        //! Paste button is accessible
        InputMethodPaste
    };

    enum PreeditFace {
        PreeditDefault,
        PreeditNoCandidates,
        PreeditKeyPress,      //!< Used for displaying the hwkbd key just pressed
        PreeditUnconvertible, //!< Inactive preedit region, not clickable
        PreeditActive         //!< Preedit region with active suggestions
    };

    //! \brief Key event request type for \a MInputContext::keyEvent().
    enum EventRequestType {
        EventRequestBoth,         //!< Both a Qt::KeyEvent and a signal
        EventRequestSignalOnly,   //!< Only a signal
        EventRequestEventOnly     //!< Only a Qt::KeyEvent
    };

    /*!
     * \brief The text format for part of the preedit string, specified by
     * start and length.
     *
     * \sa PreeditFace.
     */
    struct PreeditTextFormat {
        int start;
        int length;
        PreeditFace preeditFace;

        PreeditTextFormat()
            : start(0), length(0), preeditFace(PreeditDefault)
        {};

        PreeditTextFormat(int s, int l, const PreeditFace &face)
            : start(s), length(l), preeditFace(face)
        {};
    };
}

Q_DECLARE_METATYPE(Maliit::TextContentType)
Q_DECLARE_METATYPE(Maliit::PreeditTextFormat)

#endif
