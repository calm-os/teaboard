/* * This file is part of Maliit framework *
 *
 * Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved.
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * and appearing in the file LICENSE.LGPL included in the packaging
 * of this file.
 */

#ifndef MINPUTMETHODPLUGIN_H
#define MINPUTMETHODPLUGIN_H

#include <QObject>
#include <QStringList>
#include <QSet>

#include <maliit/namespace.h>

class MAbstractInputMethod;
class MAbstractInputMethodHost;

namespace Maliit {
namespace Plugins {

/*! \ingroup pluginapi
 * \brief An interface class for all input method plugins.
 *
 * To create a virtual keyboard / input method plugin, re-implement the virtual
 * functions and instantiate the input method implementation in the
 * createInputMethod() method. Make sure your plugin links against the m im
 * framework library as well.
 */
class InputMethodPlugin: public QObject
{
    Q_OBJECT

public:
    explicit InputMethodPlugin(QObject *parent=nullptr)
        : QObject(parent)
    {
    }

    /*! \brief Creates and returns the MAbstractInputMethod object for
     * this plugin. This function will be only called once and the allocated
     * resources will be owned by the input method server.
     */
    virtual MAbstractInputMethod *createInputMethod(MAbstractInputMethodHost *host) = 0;
};

}
}

#endif
